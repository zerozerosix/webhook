"""
A WSGI-based application to handle webhooks specifically written for Forgejo platforms.
Copyright © 2023 Benjamin Stadlbauer

Here you can perform your actions, depending on the information given in the
data dictionary. The returned string will be the plain-text body of the response
to the web-hook request.

If an exception is raised from within the function, the response will be of status 500
and the body will be the string of the exception.
"""

def handle_data(data: dict) -> str:
	return "Done"
