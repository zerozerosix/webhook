"""
A WSGI-based application to handle webhooks specifically written for Forgejo platforms.
Copyright © 2023 Benjamin Stadlbauer
"""

import os

'''
This variable stores the secret key (as a string) for calculating the signature
to compare it with the one in the request. This secret key must be equal to the
one set in the web-hook settings.
'''
SECRET_KEY = os.environ['WEBHOOK_KEY']
