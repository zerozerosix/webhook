# Forgejo Webhook WSGI app

A WSGI-based application to handle webhooks specifically written for Forgejo platforms.

## Running this WSGI app

To run the webhook app you must set the `WEBHOOK_KEY` environmental variable with the secret you have entered in the webhook setting in Forgejo. Then simply run the app with e.g.,
```
$ gunicorn app:app
```

---

Copyright © 2023 Benjamin Stadlbauer
